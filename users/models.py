import os
import uuid
from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from upload_validator import FileTypeValidator
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

def file_path(instance, filename):
    """Generic file path for new recipe image"""
    ext = filename.split('.')[-1]
    filename = f'{uuid.uuid4()}.{ext}'
    return os.path.join('uploads/files/', filename)

def file_size(value): # add this to some file where you can import it from
    limit = 0.5 * 1024 * 1024
    if value.size > limit:
        raise ValidationError('File too large. Size should not exceed 500 kb.')


class UserManager(BaseUserManager):
    def create_user(self, email, password=None, **extra_fields):
        """ create and save new user """
        if not email:
            raise ValueError('The email field must be provided')
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """create a new super user"""
        if not email:
            raise ValueError('The email field must be provided')
        user = self.create_user(
            email=self.normalize_email(email),
            password=password,
        )
        user.is_staff=True
        user.is_superuser=True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):

    INTEREST_CHOICES = [
        ('Football', 'Football'),
        ('Basketball', 'Basketball'),
        ('Ice', 'Ice'),
        ('Hockey', 'Hockey'),
        ('Motorsports', 'Motorsports'),
        ('Bandy', 'Bandy'),
        ('Skiing', 'Skiing'),
        ('Shooting', 'Shooting'),
    ]
    phone = models.CharField(max_length=20)
    last_name = models.CharField(max_length=254, default=None, null=True)
    first_name = models.CharField(max_length=255, default=None, null=True)
    username = models.CharField(max_length=255, default=None, null=True)
    email = models.EmailField(max_length=255, unique=True)
    updated = models.DateTimeField(auto_now=True)
    created = models.DateTimeField(auto_now_add=True)
    interest = models.TextField(choices=INTEREST_CHOICES, blank=True, null=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()
    USERNAME_FIELD = 'email'

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'


class UserProfile(models.Model):
    """Get users profile"""

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    image = models.FileField(upload_to=file_path, blank=True, null=True,
                               validators=[file_size, FileTypeValidator(
                                   allowed_types=['image/png', 'image/jpeg', 'image/jpg', 'application/msword',
                                                  'application/pdf', 'application/vnd.ms-excel',
                                                  'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                                                  ]
                               )
                                           ])
    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'
