from django.urls import path, include, re_path

from .views import *

app_name = "users"

urlpatterns = [
    path('', ManageUserView.as_view(), name='user'),
    path('profile/<int:pk>', ProfileUserViewSets.as_view(), name="profile"),
    path('morning', PasswordResetEmal.as_view(), name="morns"),
    path('auth/', include('dj_rest_auth.urls'), name='login'),
    path('auth/register/', include('dj_rest_auth.registration.urls'), name='register'),
]
