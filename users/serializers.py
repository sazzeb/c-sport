from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from allauth.account import app_settings as allauth_settings
from django.conf import settings

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers, exceptions
from django.contrib.auth import get_user_model, authenticate

from dj_rest_auth.registration.serializers import RegisterSerializer

from django.contrib.auth.forms import PasswordResetForm
from allauth.socialaccount.models import SocialAccount

from allauth.account.forms import ResetPasswordForm
from dj_rest_auth.serializers import PasswordResetSerializer

from users.models import *


class UserSerializer(serializers.ModelSerializer):
    """
        user serializer
    """

    profile = serializers.SerializerMethodField('get_profile')

    class Meta:
        model = get_user_model()
        fields = '__all__'
        extra_kwargs = {'password': {'write_only': True, 'min_length': 8}}

    def create(self, validated_data):
        user = get_user_model().objects.create_user(**validated_data)
        return user

    def get_profile(self, obj):
        serializer = UserProfile.objects.filter(user=obj).values().first()
        return serializer

    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""
        password = validated_data.pop('password', None)
        user = super().update(instance, validated_data)

        if password:
            user.set_password(password)
            user.save()

        return user


class UserProfileSerializer(serializers.ModelSerializer):

    """Retrieve user profile"""
    class Meta:
        model = UserProfile
        fields = ('id', 'user', 'image')

    def update(self, instance, validated_data):
        """Update a user, setting the password correctly and return it"""
        user = super().update(instance, validated_data)
        return user


class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField(required=True, allow_blank=False)
    password = serializers.CharField(style={'input_type': 'password'})

    def authenticate(self, **kwargs):
        return authenticate(self.context['request'], **kwargs)

    def _validate_email(self, email, password):
        user = None
        if email and password:
            user = self.authenticate(email=email, password=password)
        else:
            msg = _('Incorrect credential')
            raise exceptions.ValidationError({
                "errors": msg
            })

        return user

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        user = None

        if 'allauth' in settings.INSTALLED_APPS:
            from allauth.account import app_settings

            # Authentication through email
            if app_settings.AUTHENTICATION_METHOD == app_settings.AuthenticationMethod.EMAIL:
                user = self._validate_email(email, password)

        else:
            # Authentication without using allauth
            if email:
                try:
                    username = get_user_model().objects.get(email__iexact=email).get_username()
                except get_user_model().DoesNotExist:
                    pass

        # Did we get back an active user?
        if user:
            if not user.is_active:
                msg = _('This account has been suspended, contact admin to proceed.')
                raise exceptions.ValidationError({
                    "errors": msg
                })

        else:
            msg = _('Unable to log in with provided credentials.')
            raise exceptions.ValidationError({
                "errors": msg
            })

            # If required, is the email verified?
        if 'dj_rest_auth.registration' in settings.INSTALLED_APPS:
            from allauth.account import app_settings
            if app_settings.EMAIL_VERIFICATION == app_settings.EmailVerificationMethod.MANDATORY:
                email_address = user.emailaddress_set.get(email=user.email)
                if not email_address.verified:
                    raise serializers.ValidationError({
                        "errors": _('Please verify your email before you can login')
                    })

        attrs['user'] = user
        return attrs


class RegistrationSerializer(RegisterSerializer):
    """
        registration with allauth, and social login
    """
    email = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    username = serializers.CharField(required=False, write_only=True)
    first_name = serializers.CharField(required=True, write_only=True)
    last_name = serializers.CharField(required=True, write_only=True)
    phone = serializers.CharField(required=True, write_only=True)
    interest = serializers.ChoiceField(
        choices=['Football', 'Basketball', 'Ice', 'Hockey', 'Motorsports',
                 'Bandy', 'Skiing', 'Shooting'],
        style={'base_template': 'radio.html'},
        required=False
    )
    password1  = serializers.CharField(style={'input_type': 'password'}, write_only=True)
    password2 = serializers.CharField(style={'input_type': 'password'}, write_only=True)

    def validate(self, data):
        """
        Validates the password length + authenticity and dob format
        """
        pw  = data.get('password1')
        pw2 = data.get('password2')

        phone = get_user_model().objects.filter(phone=data.get('phone'))
        if phone.exists():
            raise serializers.ValidationError({
                "errors": _("The phone number already exists.")
            })
        if not data.get('interest'):
            raise serializers.ValidationError({'errors': 'Please choose sport interest'})
        if len(data.get('phone')) < 9:
            raise serializers.ValidationError({'errors': 'Incorrect phone number, check and retry'})
        if len(data.get('phone')) > 11:
            raise serializers.ValidationError({'errors': 'You have exceeded the amount number'})
        if pw != pw2:
            raise serializers.ValidationError({'errors': 'Password must match.'})
        return data

    def get_cleaned_data(self):
        super(RegistrationSerializer, self).get_cleaned_data()
        return {
            'password1': self.validated_data.get('password1', ''),
            'password2': self.validated_data.get('password2', ''),
            'email': self.validated_data.get('email', ''),
            'username': self.validated_data.get('username', ''),
            'first_name': self.validated_data.get('first_name', ''),
            'last_name': self.validated_data.get('last_name', ''),
            'phone': self.validated_data.get('phone', ''),
            'interest': self.validated_data.get('interest', ''),
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])
        first_name = self.cleaned_data.get('first_name')
        last_name = self.cleaned_data.get('last_name')
        user.last_name = first_name
        user.last_name = last_name
        user.interest = self.cleaned_data.get('interest')
        user.phone = self.cleaned_data.get('phone')
        user.save()
        UserProfile.objects.create(user=user)
        return user


# class UserPasswordResetForm(ResetPasswordForm):
#     context = {
#         "current_site": current_site,
#         "user": user,
#         "request": request,
#         "domain": getattr(settings, "APP_URL")  # frontend domain since it differs completely from my backend domain,
#         "uid": user_pk_to_url_str(user),
#         "token": temp_key,
#     }


class PasswordResetFormSerializer(PasswordResetSerializer):
    email = serializers.EmailField()
    password_reset_form_class = PasswordResetForm

    def validate_email(self, value):
        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(_('Error'))

        if not User.objects.filter(email=value).exists():
            raise serializers.ValidationError(_('Invalid e-mail address'))

        random_user = User.objects.get(email=value)
        if SocialAccount.objects.filter(user=random_user).exists():
            raise serializers.ValidationError(_('User has a social account'))

        return value
