import sys
from django.conf import settings
from django.contrib.auth import get_user_model
from rest_framework import views, generics
from django.http import HttpResponseRedirect
from rest_framework_simplejwt.authentication import JWTAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from django.contrib.sites.models import Site
from dj_rest_auth.views import PasswordResetView


from .serializers import *


class ManageUserView(generics.RetrieveUpdateAPIView):
    """Manage the authenticated user"""
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    authentication_classes = (JWTAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        """Retrieve and return authentication user"""

        return self.request.user


class ProfileUserViewSets(generics.RetrieveUpdateAPIView):

    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer

    authentication_classes = (JWTAuthentication,)
    permission_classes = (IsAuthenticated,)

    def perform_update(self, serializer):

        serializer.save()

        return self.request.user


class RegisterConfirmRedirect(views.APIView):
    def get(self, request, *args, **kwargs):
        """return to home page after email confirmation"""
        if (len(sys.argv) >= 2 and sys.argv[1] == 'runserver'):
            return HttpResponseRedirect(redirect_to='http://localhost:3000/account/login')
        else:
            return HttpResponseRedirect(redirect_to='http://c-interviewer.ml/account/login')



class PasswordResetEmal(views.APIView):
    def get(self, request, *args, **kwargs):
        """return to home page after email confirmation"""
        current_site = Site.objects.get_current()
        var = current_site.domain
        return Response('am here')
