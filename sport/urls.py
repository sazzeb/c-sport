from django.contrib import admin
from django.urls import path, include, re_path

from rest_framework import permissions
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi
from django.conf.urls.static import static
from django.conf import settings

from dj_rest_auth.registration.views import VerifyEmailView, ConfirmEmailView
from dj_rest_auth.views import PasswordResetView, PasswordResetConfirmView

from users.views import RegisterConfirmRedirect

schema_view = get_schema_view(
   openapi.Info(
      title="Testing Api for all Users",
      default_version='v1',
   ),
   public=True,
   permission_classes=(permissions.AllowAny,),
)


urlpatterns = [
    path('adminer/', admin.site.urls),
    re_path('^', include('django.contrib.auth.urls')),
    path('api/', include('users.urls', namespace='users')),
    path('', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^api/auth/accounts/', include('allauth.urls'), name='socialaccount_signup'),
    re_path(r'^api/auth/account-confirm-email/$', VerifyEmailView.as_view(), name='account_email_verification_sent'),
    re_path(r'^api/auth/account-confirm-email/(?P<key>[-:\w]+)/$', ConfirmEmailView.as_view(),
            name='account_confirm_email'),
    # path('password-reset/', PasswordResetView.as_view()),
    # path('password-reset-confirm/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), 'password_reset_confirm'),
    path('accounts/login/', RegisterConfirmRedirect.as_view(), name='redirect_after_confirm'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
